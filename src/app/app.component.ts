import { Component,Output,EventEmitter } from '@angular/core';
import {ShareService} from './share.service';
import { from } from 'rxjs';

// export interface Tile {
//   color: string;
//   cols: number;
//   rows: number;
//   text: string;
// }

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // issues:[];
  // tiles: Tile[] = [
  //   {text: 'One', cols: 2, rows: 2, color: 'lightblue'},
  //   {text: 'Two', cols: 2, rows: 1, color: 'lightgreen'},
  //   {text: 'Three', cols: 2, rows: 1, color: 'lightpink'},
  //   {text: 'Four', cols: 4, rows: 2, color: '#DDBDF1'},
  // ];

  constructor( public shareService:ShareService) { }

  ngOnInit(): void {
  }

  getInfo(){
    this.shareService.getIssuesData().subscribe((data) => {
      console.log(data);
      
    })
    
  }
 
}
