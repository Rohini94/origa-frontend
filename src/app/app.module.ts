import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { PercentageComponent } from './percentage/percentage.component';
import { UserComponent } from './user/user.component';
import { InfoComponent } from './info/info.component';
import { IgxPieChartModule } from 'igniteui-angular-charts';
import {MatGridListModule} from '@angular/material/grid-list';

// import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgApexchartsModule } from "ng-apexcharts";

// import {MaterialModule} from '@angular/material';
// import {MdButtonModule, MdCheckboxModule} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    PieChartComponent,
    PercentageComponent,
    UserComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IgxPieChartModule,
    MatGridListModule,
    NgApexchartsModule,
    HttpClientModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
